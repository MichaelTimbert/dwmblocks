//Modify this file to change what commands output to your statusbar, and recompile using the make command.
//Update Signale: update the command when this signal is received
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval in second*/	/*Update Signal*/
	{"Mem:", "free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g",	30,		0},
	{"", "sb-battery",	1,	1},
	{"", "date '+%H:%M'",					5,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
